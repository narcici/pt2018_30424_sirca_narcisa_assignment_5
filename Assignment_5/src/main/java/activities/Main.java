package activities;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

	public static void main(String args[]) {

		String fileName = "D:\\PT2018\\PT2018_30424_sirca_narcisa_assignment_5\\Assignment_5\\Activities.txt";
		List<String> content = new ArrayList<String>();
		Operations operations = new Operations();
		final String path1 = "firstmap.txt";
		final String path2 = "secondmap.txt";
		final String path3 = "filteredActivities";
		final String path4 = "activities5Minutes";
		ArrayList<MonitoredData> listMonitoredData = new ArrayList<MonitoredData>();
		// read file into stream, try-with-resources
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

			// here I put the input in a collection of String elem called <<<<<<< content
			content = stream.collect(Collectors.toList());
		} catch (IOException e) {
			e.printStackTrace();
		}
		// creates the array list containing the MonitoredData
		listMonitoredData = operations.buildMonitoredDataList(content); // works

		System.out.println("Choose the operation you want to be performed: ");
		System.out.println("0 for seing the input data as array of MonitoredData");
		System.out.println("1 for finding the number of distinct days");
		System.out.println("2 for finding the number of occurrences of each activity");
		System.out.println("3 for finding the number of occurrences of each activity on days");
		System.out.println("4 for finding the activities with a total duration > 10 hours");
		System.out.println(
				"5 for finding the activities that have 90% of the monitoring samples with duration less than 5 minutes");
		System.out.println("6 for finding the total duration of each activity");

		Scanner input = new Scanner(System.in);
		String number;
		boolean ok = true;
		while (ok == true) {
			number = input.nextLine();
			switch (number) {

			case "0":
				for (MonitoredData md : listMonitoredData) {
					System.out.println(md.getActivity_label() + "  " + md.getStart_time() + "  " + md.getEnd_time());
				}
				break;
			case "1": // no of different days here
				System.out.println(operations.noDifferentDays(listMonitoredData)); // works
				break;
			case "2": // here for each activity the total no of apparitons in total
				Map<String, Long> activityAppearanceTotalTime = operations.appeareanceActivity(listMonitoredData);
				Set<Map.Entry<String, Long>> set = activityAppearanceTotalTime.entrySet(); // from map to list
				System.out.println(set);
				operations.writeInFile(path1, set);
				break;
			case "3": // here no of appearances for each activity in each day
				Map<Integer, Map<String, Long>> secondmap = operations.dailyAppearanceActivity(listMonitoredData);
				Set<Map.Entry<Integer, Map<String, Long>>> secondset = secondmap.entrySet(); // from map to list
				secondset.forEach(x -> System.out.println(x));
				operations.writeInFile(path2, secondmap);
				break;
			case "4": // here filtered activities, having a duration > 10 h
				Map<String, Integer> filteredActivities = operations.getFilteredActivities(listMonitoredData);
				Set<Entry<String, Integer>> setFilererdActivities = filteredActivities.entrySet(); // from map to list																			
				System.out.println(filteredActivities);
				operations.writeInFile(path3, setFilererdActivities);
				break;
			case "5": // here filtered activities, that have 90% of the monitoring samples with
						// duration less than 5 minutes
				List<String> filteredList = operations.finalFilterEver(listMonitoredData);
				System.out.println(filteredList);
				operations.writeInFile(path4, filteredList);
				break;
			case "6": // here total duration of each activity
				Map<String, Integer> durationsMap = operations.getIntFromSeconds(listMonitoredData);
				System.out.println(durationsMap);
				break;
			/*
			 * exception is thrown because there exist dublicates case "6": Map<String,
			 * Long> dailyDuration= operations.getDailyDuration(listMonitoredData);
			 * System.out.println(dailyDuration); break;
			 */
			default:
				System.out.println("Done!");
				ok = false;
				break;
			}
		}
		input.close();

	}
}

package activities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalTime;

import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.joda.time.format.DateTimeFormat;

public class MonitoredData {

	private String start_time;
	private String end_time;
	private String activity_label;

	DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public MonitoredData(String s) {
		String[] temporal = s.split("[	]+");
		this.start_time = temporal[0];
		this.end_time = temporal[1];
		this.activity_label = temporal[2];
	}

	public String getStart_time() {
		return start_time;
	}

	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}

	public String getEnd_time() {
		return end_time;
	}

	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}

	public String getActivity_label() {
		return activity_label;
	}

	public void setActivity_label(String activity_label) {
		this.activity_label = activity_label;
	}

	public String getDate() {
		String[] imparte = this.start_time.split("[ ]"); // split by space
		return imparte[0]; // first part is the date
	}

	public long getDuration() {
		Duration duration = null;
		String[] inceput = this.start_time.split("[ ]");
		String[] sfarsit = this.end_time.split("[ ]");
		LocalTime timpInceput = LocalTime.parse(inceput[1]);
		LocalTime timpSfarsit = LocalTime.parse(sfarsit[1]);
		duration = Duration.between(timpInceput, timpSfarsit); // duration is in sec

		if (inceput[0].compareTo(sfarsit[0]) == 0) { // they belong to the same day
			return duration.getSeconds();
		} else

			return duration.getSeconds() + 24 * 60 * 60; //
	}

	public DateTime getStart_DayTime() {
		DateTime date_start = DateTime.parse(this.start_time, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
		return date_start;
	}

	public DateTime getEnd_DayTime() {
		DateTime date_start = DateTime.parse(this.end_time, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
		return date_start;
	}

	public int getSecondsDurations() { // joda-time library can work with Seconds
		return Seconds.secondsBetween(getStart_DayTime(), getEnd_DayTime()).getValue(0); // int from seconds
	}

	public int getDay() { // here only the day, drom a date format
		String[] inceput = this.start_time.split("[ ]");
		String[] day = inceput[0].split("[-]");
		return Integer.parseInt(day[2]);
	}
}
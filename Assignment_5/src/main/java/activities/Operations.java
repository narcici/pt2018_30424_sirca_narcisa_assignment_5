package activities;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Operations {

	// get the MonitoredData array here
	public ArrayList<MonitoredData> buildMonitoredDataList(List<String> stringList) {
		ArrayList<MonitoredData> md = new ArrayList<MonitoredData>();
		stringList.stream().forEach(x -> md.add(new MonitoredData(x))); // use stream to create the list of
																		// MonitoredData
		return md;
	}

	// counts the different days here
	public int noDifferentDays(ArrayList<MonitoredData> monitoredDataList) {
		return (int) monitoredDataList.stream().map(x -> x.getDate()).distinct().count();
	}

	// counts the no of appearances for each activity
	public Map<String, Long> appeareanceActivity(ArrayList<MonitoredData> monitoredDataList) {
		Map<String, Long> result = monitoredDataList.stream()
				.collect(Collectors.groupingBy(MonitoredData::getActivity_label, Collectors.counting()));
		return result;
	}

	// map integer day, string activity_label integer noAppearance on that day
	public Map<Integer, Map<String, Long>> dailyAppearanceActivity(ArrayList<MonitoredData> monitoredDataList) {
		Map<Integer, Map<String, Long>> mapping = monitoredDataList.stream().collect(Collectors.groupingBy(
				MonitoredData::getDay, Collectors.groupingBy(MonitoredData::getActivity_label, Collectors.counting())));
		return mapping;
	}

	// daily duration of each activity
	public Map<String, Long> getDailyDuration(ArrayList<MonitoredData> monitoredDataList) {
		return (Map<String, Long>) monitoredDataList.stream()
				.collect(Collectors.toMap(MonitoredData::getActivity_label, m -> m.getDuration()));
	}

	// total duration of each activity
	public Map<String, Integer> getIntFromSeconds(ArrayList<MonitoredData> monitoredDataList) {
		return monitoredDataList.stream().collect(Collectors.groupingBy(MonitoredData::getActivity_label,
				Collectors.summingInt(MonitoredData::getSecondsDurations)));
	}

	// filtered activities ,String +integer no of hours
	public Map<String, Integer> getFilteredActivities(ArrayList<MonitoredData> monitoredDataList) {
		// public Map<String, DateTime> getFilteredActivities(ArrayList<MonitoredData>
		// monitoredDataList){

		return monitoredDataList.stream()
				.collect(Collectors.groupingBy(MonitoredData::getActivity_label,
						Collectors.summingInt(MonitoredData::getSecondsDurations)))
				.entrySet().stream().filter(m -> m.getValue() > 36000)
				.collect(Collectors.toMap(m -> m.getKey(), m -> m.getValue() / 3600));
		// .collect(Collectors.toMap(m->m.getKey(), new DateTime(2018, 5, 22, m->
		// m.getValue()/3600, m-> (m.getValue() %3600)/60, m-> m.getValue() %3600)%60 );
	}

	public List<String> finalFilterEver(ArrayList<MonitoredData> listMonitoredData) {
		Map<String, Long> filteredMap = (Map<String, Long>) listMonitoredData.stream()
				.filter(e -> e.getSecondsDurations() < 300)
				.collect(Collectors.groupingBy(e -> e.getActivity_label(), Collectors.counting()));

		Map<String, Long> numberedAppearances = appeareanceActivity(listMonitoredData); // from some previous task

		List<String> finalOne = new ArrayList<String>();
		filteredMap.entrySet().stream().filter(e -> e.getValue() >= 0.9 * numberedAppearances.get(e.getKey()))
				.forEach(e -> finalOne.add(e.getKey()));
		return finalOne;
	}

	// write the set into specified file
	public void writeInFile(String pathOfFile, Object o) {
		BufferedWriter text = null;
		FileWriter writer = null;
		try {
			// File file=new File(pathOfFile); // here we give the path of the file
			// if(file.createNewFile()) { // if it was created
			writer = new FileWriter(pathOfFile);
			text = new BufferedWriter(writer);
			text.write(o.toString());
			System.out.println("the set was written into the file " + pathOfFile);
			// }
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (text != null)
					text.close();
				if (writer != null)
					writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}

/*
 * public Map<String, Long> filteredActivities(Set<Entry<String, Long>>
 * setDurations) {
 * 
 * @SuppressWarnings("unchecked") Map<String, Long> filteredMap=(Map<String,
 * Long>) ((Stream<Entry<String,Long>>) setDurations.stream()
 * .collect(Collectors.toMap(Entry::getKey, Entry::getValue))) .filter(p->
 * p.getValue() > 36000); return filteredMap; } //here only remain the filtered
 * activities public Map<String, Long> filteredSecond(Set<Map.Entry<String,
 * Long>> setDurations){
 * 
 * @SuppressWarnings("unchecked") Set<Map.Entry<String, Long>> resultedSet=
 * (Set<Entry<String, Long>>) setDurations.stream() //ArrayList<Entry<String,
 * Long>> resultedSet= (ArrayList<Entry<String, Long>>) setDurations.stream()
 * .filter(p -> p.getValue() > 36000); // total duration larger that 10 hours
 * Map<String, Long> filteredMap= new HashMap<String, Long>();
 * for(Map.Entry<String, Long> m:resultedSet) { filteredMap.put(m.getKey(),
 * m.getValue()); } return filteredMap; }
 */

/*
 * 
 * 
 * public LocalDateTime getDateTime(MonitoredData md) { long
 * value=md.getDuration(); LocalDateTime triggerTime
 * =LocalDateTime.ofInstant(Instant.ofEpochSecond(value),
 * TimeZone.getDefault().toZoneId()); return triggerTime; }
 * 
 * public LocalDateTime getDateTime(ArrayList<MonitoredData> md) { long
 * value=md.getDuration(); LocalDateTime triggerTime
 * =LocalDateTime.ofInstant(Instant.ofEpochSecond(value),
 * TimeZone.getDefault().toZoneId()); return triggerTime; }
 */
